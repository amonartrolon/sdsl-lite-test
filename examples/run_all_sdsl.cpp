#include <sdsl/bit_vectors.hpp>
#include <sdsl/io.hpp>
#include <sdsl/util.hpp>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace sdsl;

void run_bit_vector_access(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b[p]; 
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector Access Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_v(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  rank_support_v<0,1> b_rank_v(&b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_rank_v(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector RANK_V RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_v5(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  rank_support_v5<0,1> b_rank_v5(&b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_rank_v5(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector RANK_V5 RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_scan(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  rank_support_scan<> b_rank_scan(&b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_rank_scan(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  cout << "chegou aqui patrao" << endl;
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector RANK_SCAN RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_rrr(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  rrr_vector<127> rrrb(b);
  rrr_vector<127>::rank_1_type rrr_rank(&rrrb);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = rrr_rank(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector RRR RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_il(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  bit_vector_il<512> il_b(b);
  rank_support_il<> b_rank_il(&il_b);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_rank_il(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector IL RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_sd(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;

  sd_vector<> sd_b(b);
  rank_support_sd<> sd_rank(&sd_b);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = sd_rank(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SD RANK Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_rank_hyb(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  hyb_vector<> hyb_b(b);
  rank_support_hyb<> hyb_rank(&hyb_b);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = hyb_rank(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector HYB RANK Total time(s): " << elapsed.count() << endl;
}


void get_size_plain_bit_vector(bit_vector &b) {
    rank_support_v<0,1> b_rank_v(&b);
    rank_support_v5<0,1> b_rank_v5(&b);
    rank_support_scan<0,1> b_rank_scan(&b);
    select_support_mcl<> b_select_mcl(&b);
    select_support_scan<0,1> b_select_scan(&b);
    cout << "rank_v select_mcl BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_v) + size_in_bytes(b_select_mcl) << endl;
    cout << "rank_v select_scan BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_v) + size_in_bytes(b_select_scan) << endl;
    cout << "rank_v5 select_mcl BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_v5) + size_in_bytes(b_select_mcl) << endl;
    cout << "rank_v5 select_scan BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_v5) + size_in_bytes(b_select_mcl) << endl;
    cout << "rank_scan select_mcl BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_scan) + size_in_bytes(b_select_mcl) << endl;
    cout << "rank_scan select_scan BitVector Size in bytes: " << size_in_bytes(b) +  size_in_bytes(b_rank_scan) + size_in_bytes(b_select_scan) << endl;
}



void run_bit_vector_il_access(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  bit_vector_il<> il_b(b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = il_b[p]; 
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Il BitVector Access Total time(s): " << elapsed.count() << endl;
}

void get_size_plain_il_bit_vector(bit_vector &b) {
    bit_vector_il<> il_b(b);
    rank_support_il<> b_rank_il(&il_b);
    select_support_il<> b_select_il(&il_b);
    //cout << "IL BitVector Size in bytes: " << size_in_bytes(il_b) << endl;
    //cout << "rank il BitVector Size in bytes: " <<  size_in_bytes(b_rank_il) << endl;
    //cout << "select il BitVector Size in bytes: " <<  size_in_bytes(b_select_il) << endl;
    cout << "IL BitVector Size in bytes: " << size_in_bytes(il_b) +  size_in_bytes(b_rank_il) + size_in_bytes(b_select_il) << endl;
}

void run_rrr_bit_vector_access(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  rrr_vector<127> rrrb(b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = rrrb[p]; 
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "RRR BitVector Access Total time(s): " << elapsed.count() << endl;
}

void get_size_rrr_bit_vector(bit_vector &b) {
  rrr_vector<127> rrrb(b);
  rrr_vector<127>::select_1_type rrr_select(&rrrb);
  rrr_vector<127>::rank_1_type rrr_rank(&rrrb);
  //rank_support_rrr<> rrr_rank(rrrb);
  //select_support_rrr<> rrr_select(rrrb);
  //cout << "RRR BitVector Size in bytes: " << size_in_bytes(rrrb) << endl; 
  //cout << "RRR BitVector Size with rank in bytes: " << size_in_bytes(rrr_rank) << endl; 
  //cout << "RRR BitVector Size with select in bytes: " << size_in_bytes(rrr_select) << endl; 
  cout << "RRR BitVector Size in bytes: " << size_in_bytes(rrrb) +  size_in_bytes(rrr_rank) + size_in_bytes(rrr_select) << endl;
}

void run_sd_bit_vector_access(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  sd_vector<> sd_b(b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = sd_b[p]; 
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "SD BitVector Access Total time(s): " << elapsed.count() << endl;
}

void get_size_sd_bit_vector(bit_vector &b) {
  sd_vector<> sd_b(b);
  rank_support_sd<> sd_rank(&sd_b);
  select_support_sd<> sd_select(&sd_b);
  //rank_support_rrr<> rrr_rank(rrrb);
  //select_support_rrr<> rrr_select(rrrb);
  //cout << "SD BitVector Size in bytes: " << size_in_bytes(sd_b) << endl; 
  //cout << "SD BitVector Size with rank in bytes: " << size_in_bytes(sd_rank) << endl; 
  //cout << "SD BitVector Size with select in bytes: " << size_in_bytes(sd_select) << endl; 
  cout << "SD BitVector Size in bytes: " << size_in_bytes(sd_b) +  size_in_bytes(sd_rank) + size_in_bytes(sd_select) << endl;
}

void run_hyb_bit_vector_access(bit_vector &b, vector<unsigned int> &v_query) {
  std::vector<int> results;
  hyb_vector<> hyb_b(b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = hyb_b[p]; 
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Hyb BitVector Access Total time(s): " << elapsed.count() << endl;
}

void get_size_hyb_bit_vector(bit_vector &b) {
  hyb_vector<> hyb_b(b);
  rank_support_hyb<> hyb_rank(&hyb_b);
  //rank_support_rrr<> rrr_rank(rrrb);
  //select_support_rrr<> rrr_select(rrrb);
  //cout << "Hyb BitVector Size in bytes: " << size_in_bytes(hyb_b) << endl; 
  //cout << "Hyb BitVector Size with rank in bytes: " << size_in_bytes(hyb_rank) << endl; 
  cout << "Hyb BitVector Size in bytes: " << size_in_bytes(hyb_b) +  size_in_bytes(hyb_rank) << endl;
}

void get_all_bitvector_size(bit_vector &b) {
    get_size_plain_bit_vector(b);
    get_size_rrr_bit_vector(b);
    get_size_sd_bit_vector(b);
    get_size_hyb_bit_vector(b);
    get_size_plain_il_bit_vector(b);
}


void run_all_access(bit_vector &b, std::string access_file_name) {
    std::ifstream query(access_file_name);
    vector<unsigned int> v_query;
    unsigned int j;
    while (query >> j) {
        v_query.push_back(j);
    }

    run_bit_vector_access(b, v_query);
    run_rrr_bit_vector_access(b, v_query);
    run_sd_bit_vector_access(b, v_query);
    run_hyb_bit_vector_access(b, v_query);
    run_bit_vector_il_access(b, v_query);
}

void run_all_rank(bit_vector &b, std::string rank_file_name) {
    std::ifstream query(rank_file_name);
    vector<unsigned int> v_query;
    unsigned int j;
    unsigned int r;
    while (query >> j >> r) {
        v_query.push_back(j);
    }

    run_bit_vector_rank_v(b, v_query);
    run_bit_vector_rank_v5(b, v_query);
    // TODO()
    //run_bit_vector_rank_scan(b, v_query);
    run_bit_vector_rank_rrr(b, v_query);
    run_bit_vector_rank_sd(b, v_query);
    run_bit_vector_rank_hyb(b, v_query);
    run_bit_vector_rank_il(b, v_query);
}

void run_bit_vector_select_mcl(bit_vector &b, vector<unsigned long> &v_query) {
  std::vector<int> results;
  select_support_mcl<> b_select_mcl(&b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_select_mcl(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SELECT_MCL SELECT Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_select_scan(bit_vector &b, vector<unsigned long> &v_query) {
  std::vector<int> results;
  select_support_scan<0,1> b_select_scan(&b);
  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_select_scan(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SELECT_SCAN SELECT Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_select_il(bit_vector &b, vector<unsigned long> &v_query) {
  std::vector<int> results;
  bit_vector_il<> il_b(b);
  select_support_il<> b_select_il(&il_b);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = b_select_il(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SELECT_IL SELECT Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_select_rrr(bit_vector &b, vector<unsigned long> &v_query) {
  std::vector<int> results;
  rrr_vector<127> rrrb(b);
  rrr_vector<127>::select_1_type rrr_select(&rrrb);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = rrr_select(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SELECT_RRR SELECT Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_select_sd(bit_vector &b, vector<unsigned long> &v_query) {
  std::vector<int> results;

  sd_vector<> sd_b(b);
  select_support_sd<> sd_select(&sd_b);

  auto first = std::chrono::high_resolution_clock::now();
  auto total_time = std::chrono::high_resolution_clock::now();
  for (auto p : v_query) {
    auto t0 = std::chrono::high_resolution_clock::now();
    int c = sd_select(p);
    auto t1 = std::chrono::high_resolution_clock::now();
    total_time += t1-t0;
    results.push_back(c);
  }
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "Plain BitVector SELECT_SD SELECT Total time(s): " << elapsed.count() << endl;
}

void run_all_select(bit_vector &b, std::string select_file_name) {
    std::ifstream query(select_file_name);
    vector<unsigned long> v_query;
    unsigned long j;
    unsigned long r;
    while (query >> j >> r) {
        v_query.push_back(j);
    }

    run_bit_vector_select_mcl(b, v_query);
    //run_bit_vector_select_scan(b, v_query);
    run_bit_vector_select_rrr(b, v_query);
    run_bit_vector_select_sd(b, v_query);
    cout << "IL NOT DEFINED" << endl;
    run_bit_vector_select_il(b, v_query);
}

void run_bit_vector_rrr_compression(bit_vector &b, unsigned long p) {
  auto first = std::chrono::high_resolution_clock::now();
  rrr_vector<127> rrrb(b);
  rrr_vector<127>::select_1_type rrr_select(&rrrb);
  rrr_vector<127>::rank_1_type rrr_rank(&rrrb);
  auto total_time = std::chrono::high_resolution_clock::now();

  int c = rrr_select(p);
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "RRR COMPRESSION TIME: " << elapsed.count() << endl;
}

void run_bit_vector_sd_compression(bit_vector &b, unsigned long p) {
  auto first = std::chrono::high_resolution_clock::now();
  sd_vector<> sd_b(b);
  rank_support_sd<> sd_rank(&sd_b);
  select_support_sd<> sd_select(&sd_b);
  auto total_time = std::chrono::high_resolution_clock::now();
  int c = sd_select(p);
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "SD Compression Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_hyb_compression(bit_vector &b, unsigned long p) {
  auto first = std::chrono::high_resolution_clock::now();
  hyb_vector<> hyb_b(b);
  rank_support_hyb<> hyb_rank(&hyb_b);
  auto total_time = std::chrono::high_resolution_clock::now();
  int c = hyb_rank(p);
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "NO SELECT HYB Compression Total time(s): " << elapsed.count() << endl;
}

void run_bit_vector_il_compression(bit_vector &b, unsigned long p) {
  auto first = std::chrono::high_resolution_clock::now();
  bit_vector_il<512> il_b(b);
  rank_support_il<> b_rank_il(&il_b);
  select_support_il<> b_select_il(&il_b);
  auto total_time = std::chrono::high_resolution_clock::now();
  int c = b_select_il(p);
  std::chrono::duration<double> elapsed = total_time - first;
  cout << "IL Compression Total time(s): " << elapsed.count() << endl;
}

void run_all_compress(bit_vector &b) {
  run_bit_vector_rrr_compression(b, 10);
  run_bit_vector_sd_compression(b, 10);
  run_bit_vector_hyb_compression(b, 10);
  run_bit_vector_il_compression(b, 10);
}

int main(int argc, char *argv[])
{
    std::string filename_prefix(argv[1]);
    std::ifstream f(filename_prefix, std::ios::binary| std::ios::ate);
    char c;
    unsigned long int size = f.tellg();
    cout << "Size: " << size << endl;
    bit_vector b(size, 0);
    f.seekg(0);
    int i = 0;
    while ( !f.eof() ) {
      f.get(c);
      if (c == '2')
        b[i] = 1;
      if (c == '\n')
        break;
      ++i;
    }
    /*
    get_all_bitvector_size(b);
    run_all_access(b, argv[2]);

    run_all_rank(b, argv[3]);
    run_all_select(b, argv[4]);
    */
    run_all_compress(b);
    return 0;
}
